angular.module('myApp').controller('appController', function($scope, appService) {

    //variables
    $scope.filterList = appService.filterList;
    $scope.footerLinkList = appService.footerLinkList;
    $scope.index = appService.response.length;
    $scope.notifications = [];

    var current = 0,
        limit = 5;
    $scope.notifications = appService.response.slice(current, current + limit);
    current += limit;

    //methods
    $scope.loadMore = function() {
      $scope.notifications.push(appService.response.slice(current, current + limit));
    };
});
